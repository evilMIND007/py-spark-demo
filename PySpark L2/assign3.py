import string

from pyspark import SparkContext
from pyspark.sql import SparkSession
from pyspark.sql.types import *
import pyspark.sql.functions as F


def download_type(value):
    if int(value) < 100000:
        return 'Small'
    elif 100000 < int(value) < 1000000:
        return 'Medium'
    else:
        return 'Large'


def assignment_3():
    # sc = SparkContext(master="local[*]", appName="assignment-3")
    spark = SparkSession.builder \
        .master("local[*]") \
        .appName("assignment-3") \
        .getOrCreate()
    sc = spark.sparkContext

    # Read the file as RDD
    _log_file_RDD = sc.textFile("D:\\Projects\\Projects\\Python\\PySpark\\PySpark L2\\in\\log_file.txt") \
        .map(lambda x: ','.join([''.join(e for e in y if e in string.printable).strip('\"') for y in x.split(',')]))

    _log_file_Data = _log_file_RDD.map(lambda x: x.split("\n"))  # Split the file line-wise
    _log_header = _log_file_Data.first()  # First Row Of File is Header
    _log_data = _log_file_Data.filter(lambda x: x != _log_header).map(lambda x: x[0].split(","))

    # print(_log_data.collect())

    """
    CREATE THE DATAFRAME SCHEMA.
    """
    _schema = StructType([
        StructField("date", StringType(), True),
        StructField("time", StringType(), True),
        StructField("size", StringType(), True),
        StructField("r_version", StringType(), True),
        StructField("r_arch", StringType(), True),
        StructField("r_os", StringType(), True),
        StructField("package", StringType(), True),
        StructField("version", StringType(), True),
        StructField("country", StringType(), True),
        StructField("ip_id", StringType(), True)
    ])
    _logData_DF = spark.createDataFrame(_log_data, _schema)
    # _logData_DF.show()

    _download_type_UDF = F.udf(download_type, StringType())
    _logData_with_DownloadType_DF = _logData_DF.withColumn("download_type", _download_type_UDF(F.col("size")))
    # _logData_with_DownloadType_DF.show()

    _final_DF = _logData_with_DownloadType_DF.groupby(F.col("country"), F.col("download_type")).count()
    _final_DF.show()

    _final_DF.write.mode('overwrite').parquet("D:\\Projects\\Projects\\Python\\PySpark\\PySpark L2\\out\\assign3\\final.parquet")


if __name__ == '__main__':
    assignment_3()
