from itertools import count

from pyspark.sql import SparkSession
from pyspark.sql import functions as F


def assignment_2():
    spark = SparkSession.builder \
        .master("local[*]") \
        .appName("assignment-2") \
        .getOrCreate()
    log_df = spark.read.option("header", True).csv(
        "D:\\Projects\\Projects\\Python\\PySpark\\PySpark L2\\in\\log_file.txt")
    log_df.show()
    country_df = spark.read.option("header", True).csv(
        "D:\\Projects\\Projects\\Python\\PySpark\\PySpark L2\\in\\country.txt")
    country_df.show()

    _broad_c = F.broadcast(country_df)

    _count_df = log_df.groupBy(F.col("country")).count()
    _count_df.show()
    _join_data_df = _count_df.join(F.broadcast(_broad_c), _count_df.country == _broad_c.country_code)
    _final_df = _join_data_df.select(F.col("country_code").alias("Country_Code"),
                                     F.col("country_name").alias("Country Name"),
                                     F.col("count").alias("Total Downloads"))
    _final_df.show()
    _final_df.write.mode("overwrite").csv("D:\\Projects\\Projects\\Python\\PySpark\\PySpark L2\\out\\assign2\\final.txt")


if __name__ == '__main__':
    assignment_2()
