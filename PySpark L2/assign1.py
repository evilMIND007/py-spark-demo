from itertools import count
from pyspark.sql import SparkSession
from pyspark.sql.functions import *
from pyspark.sql.types import *


def assignment_1():
    spark = SparkSession.builder \
        .master("local[1]") \
        .appName("assignment-1") \
        .getOrCreate()
    logDF = spark.read.option("header", True).csv(
        "D:\\Projects\\Projects\\Python\\PySpark\\PySpark L2\\in\\log_file.txt")
    # logDF.show()

    filteredDF = logDF.filter((logDF.package != 'NA') | (logDF.version != 'NA'))
    filteredDF.show()

    countDownloadDF = filteredDF.groupBy("package") \
                .agg(count("package").alias("No of Downloads"))
    countDownloadDF.show()

    avgDF = filteredDF.groupBy("country") \
            .agg({'size' : 'avg'})
    avgDF.show()


if __name__ == '__main__':
    assignment_1()
