from pyspark.sql import SparkSession
from pyspark.sql import functions as F


def assignment_4():
    spark = SparkSession.builder \
        .master("local[*]") \
        .appName("assignment-4") \
        .getOrCreate()
    _log_df = spark.read.format("csv").option("header", True).load(
        "D:\\Projects\\Projects\\Python\\PySpark\\PySpark L2\\in\\log_file.txt")
    # _log_df.show()
    _log_df.write.mode("overwrite").json(
        "D:\\Projects\\Projects\\Python\\PySpark\\PySpark L2\\out\\assign4\\log_json.json")

    _new_log_df = spark.read.format("json").load(
        "D:\\Projects\\Projects\\Python\\PySpark\\PySpark L2\\out\\assign4\\log_json.json")
    # _new_log_df.show()

    _group_by_date_df = _new_log_df.groupby("date").agg(F.max(_new_log_df.size), F.min(_new_log_df.size),
                                                        F.avg(_new_log_df.size))
    # _group_by_date_df.show()
    _max_min_avg_df = _group_by_date_df.select(F.col("date"), F.col("max(size)").alias("Max Download Size"),
                                               F.col("min(size)").alias("Min Download Size"),
                                               F.col("avg(size)").alias("Avg Download Size"))
    # _max_min_avg_df.show()

    _sorted_df = _new_log_df.sort(F.col("date").asc())
    # _sorted_df.show()

    """
    SAVE OUTPUTS TO JSON FILE
    """
    _max_min_avg_df.write.mode("overwrite").json(
        "D:\\Projects\\Projects\\Python\\PySpark\\PySpark L2\\out\\assign4\\max_min_avg.json")
    _sorted_df.write.mode("overwrite").json(
        "D:\\Projects\\Projects\\Python\\PySpark\\PySpark L2\\out\\assign4\\sorted_by_date_asc.json")


if __name__ == '__main__':
    assignment_4()
