from pyspark import SparkContext

def assignment_5():
    sc = SparkContext(master="local[1]", appName="assignment 4")

    realEstateFile = sc.textFile("D:\\Projects\\Projects\\Python\\PySpark\\PySpark L1\\in\\realestate.txt")
    realHeader = realEstateFile.first()
    realDataRDD = realEstateFile.filter(lambda x: x != realHeader)
    splitRealDataRDD = realDataRDD.map(lambda x: x.split("|"))

    # Property with 3 Bed Rooms
    _3_bedRoomsRDD = splitRealDataRDD.filter(lambda x: x if(x[3] == "3") else None)
    # print(_3_bedRoomsRDD.map(lambda x: x[0]).collect())
    final_3_bedRDD = _3_bedRoomsRDD.map(lambda x: x[0])
    print("Property IDs with 3 Bed Rooms --> {}".format(final_3_bedRDD.collect()))

    #Properties with atleast 2 Bath Room
    _2_bathRoomsRDD = splitRealDataRDD.filter(lambda x: x if(int(x[4]) >= 2) else None)
    final_2_bathRDD = _2_bathRoomsRDD.map(lambda x: x[0])
    print("Property IDs with atleast 2 Bath Rooms --> {}".format(final_2_bathRDD.collect()))

    # Properties with 3 Bed Room and atleast 2 Bath Room
    _3_bed_2_bathRoomRDD = final_3_bedRDD.intersection(final_2_bathRDD)

    print("Property IDs with 3 Bed Rooms and atleast 2 Bath Rooms --> {}".format(_3_bed_2_bathRoomRDD.collect()))

    final_3_bedRDD.saveAsTextFile("D:\Projects\Projects\Python\PySpark\PySpark L1\out\\assign5\\3_bed_RDD")
    final_2_bathRDD.saveAsTextFile("D:\Projects\Projects\Python\PySpark\PySpark L1\out\\assign5\\2_bath_RDD")
    _3_bed_2_bathRoomRDD.saveAsTextFile("D:\Projects\Projects\Python\PySpark\PySpark L1\out\\assign5\\3_bed_2_bath_RDD")

if __name__ == '__main__':
    assignment_5()
