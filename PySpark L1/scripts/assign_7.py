from pyspark import SparkContext
from string import digits
import re

def assignment_7():
    sc = SparkContext(master="local[1]", appName="assignment 4")

    workFile = sc.textFile("D:\\Projects\\Projects\\Python\\PySpark\\PySpark L1\\in\\sampledata.txt")
    final_File_RDD = workFile.flatMap(lambda x: x.split(" "))

    # print(final_File_RDD.collect())

    # Remove Punctuations
    _noMarksRDD = final_File_RDD.map(lambda x: re.sub(r'[^\w\s]', '', x))
    # print(_noMarksRDD.collect())

    # Remove Numeric Values
    _noNumericRDD = _noMarksRDD.map(lambda x: x.translate(str.maketrans('', '', digits)))
    # print(_noNumericRDD.collect())

    # Count Words.
    _countRDD = _noNumericRDD.countByValue()
    print("Occorrences of each Words is --> {}".format(_countRDD))

if __name__ == '__main__':
    assignment_7()