from pyspark import SparkContext

def assignment_3():
    sc = SparkContext(master="local[1]", appName="assignment 3")

    realEstateFile = sc.textFile("D:\\Projects\\Projects\\Python\\PySpark\\PySpark L1\\in\\realestate.txt")
    realHeader = realEstateFile.first()
    realDataRDD = realEstateFile.filter(lambda x: x != realHeader)
    splitRealDataRDD = realDataRDD.map(lambda x: x.split("|"))

    resultRDD = splitRealDataRDD.map(lambda x: ("Property ID : {}, Location : {}, Price : {}".format(x[0],x[1],float(x[5])*float(x[6]))))
    print(resultRDD.take(10))

if __name__ == '__main__':
    assignment_3()