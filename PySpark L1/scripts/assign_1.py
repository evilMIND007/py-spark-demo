from pyspark import SparkContext

def assignment_1():
    sc = SparkContext(master="local[1]", appName="assignment 1")

    realEstateFile = sc.textFile("D:\\Projects\\Projects\\Python\\PySpark\\PySpark L1\\in\\realestate.txt")
    realHeader = realEstateFile.first()
    # print(realHeader)
    realDataRDD = realEstateFile.filter(lambda x : x != realHeader)
    # print(realDataRDD.collect())
    splitRealDataRDD = realDataRDD.map(lambda x : x.split("|"))
    resultRDD = splitRealDataRDD.filter(lambda x : x[1] == "Thomas County")
    print(resultRDD.collect())

if __name__ == '__main__':
    assignment_1()