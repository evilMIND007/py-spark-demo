from pyspark import SparkContext

def assignment_6():
    sc = SparkContext(master="local[1]", appName="assignment 4")

    realEstateFile = sc.textFile("D:\\Projects\\Projects\\Python\\PySpark\\PySpark L1\\in\\realestate.txt")
    realHeader = realEstateFile.first()
    realDataRDD = realEstateFile.filter(lambda x: x != realHeader)
    splitRealDataRDD = realDataRDD.map(lambda x: x.split("|"))

    final_result_RDD = splitRealDataRDD.filter(lambda x: x if(x[3] == "3" and int(x[4]) >= 2 and float(x[5])*float(x[6]) <=500000) else None)
    # print(final_result_RDD)
    final_result_RDD.saveAsTextFile("D:\\Projects\\Projects\\Python\\PySpark\\PySpark L1\\out\\assign6\\resultRDD")


if __name__ == '__main__':
    assignment_6()
