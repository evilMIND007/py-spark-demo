from pyspark import SparkContext
from operator import add

def assignment_4():
    sc = SparkContext(master="local[1]", appName="assignment 4")

    realEstateFile = sc.textFile("D:\\Projects\\Projects\\Python\\PySpark\\PySpark L1\\in\\realestate.txt")
    realHeader = realEstateFile.first()
    realDataRDD = realEstateFile.filter(lambda x: x != realHeader)
    splitRealDataRDD = realDataRDD.map(lambda x: x.split("|"))

    locationPairRDD = splitRealDataRDD.map(lambda x: (x[1],1))
    countLocationRDD = locationPairRDD.reduceByKey(add).collect()
    print(countLocationRDD)

if __name__ == '__main__':
    assignment_4()