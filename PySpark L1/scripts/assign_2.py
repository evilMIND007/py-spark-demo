from pyspark import SparkContext

def assignment_2():
    sc = SparkContext(master="local[1]", appName="assignment 2")

    realEstateFile = sc.textFile("D:\\Projects\\Projects\\Python\\PySpark\\PySpark L1\\in\\realestate.txt")
    realHeader = realEstateFile.first()
    realDataRDD = realEstateFile.filter(lambda x : x != realHeader)
    splitRealDataRDD = realDataRDD.map(lambda x: x.split("|"))

    uniqueLocationRDD = splitRealDataRDD.map(lambda x: x[1])
    print(list(set(uniqueLocationRDD.collect())))

if __name__ == '__main__':
    assignment_2()